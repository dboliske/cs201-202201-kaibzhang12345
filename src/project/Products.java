//Role of this class:The main function of this category is to add products, delete products, modify products, query all users, query by user name
//Author:Zhang Kai
//Date:May 10th, 2022



package IITShopSystem;

import java.util.Scanner;

public class Products {
	int pnum;
	String pname;
	double pprice;
	
	
	public int getPnum() {
		return pnum;
	}


	public void setPnum(int pnum) {
		this.pnum = pnum;
	}


	public String getPname() {
		return pname;
	}


	public void setPname(String pname) {
		this.pname = pname;
	}


	public double getPprice() {
		return pprice;
	}


	public void setPprice(double pprice) {
		this.pprice = pprice;
	}


	
	
	@Override
	public String toString() {
		return "Products [pnum=" + pnum + ", pname=" + pname + ", pprice=" + pprice + "]";
	}

	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pname == null) ? 0 : pname.hashCode());
		result = prime * result + pnum;
		long temp;
		temp = Double.doubleToLongBits(pprice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Products other = (Products) obj;
		if (pname == null) {
			if (other.pname != null)
				return false;
		} else if (!pname.equals(other.pname))
			return false;
		if (pnum != other.pnum)
			return false;
		if (Double.doubleToLongBits(pprice) != Double.doubleToLongBits(other.pprice))
			return false;
		return true;
	}


	/*
	 *Products added 
     */
	public boolean addProducts(Products[] psArray,int pcount){	 
	
		boolean flag=false;
		
		boolean flag2=false;
		Scanner sc=new Scanner(System.in);
		do{
			System.out.print("Please enter the item number.");
			int num=sc.nextInt();
			int sn=-1;
			for(int i=0;i<pcount;i++){
				if(num==psArray[i].pnum){
					sn=i;
					break;
				}
			}
			if(sn>=0){
				System.out.println("This item number already exists, please add a new item!");
				flag2=true;
			}else{
				System.out.print("Please enter the product name.");
				String name=sc.next();
				System.out.print("Please enter the price of the item.");
				double price=sc.nextDouble();
					Products ps=new Products();
					ps.pname=name;
					ps.pnum=num;
					ps.pprice=price;
				
					psArray[pcount]=ps;
					flag=true;
					flag2=false;
				}	
	}while(flag2);
	return flag;
	}
	
	
public boolean deleteProducts(Products[] psArray,int pcount){
		
		boolean flag=false;
	
		boolean flag1=false;
		do{
			Scanner sc=new Scanner(System.in);
			System.out.print("Please enter the item number to be deleted.");
			int num=sc.nextInt();
			int sn=-1;
			for(int i=0;i<pcount;i++){
				if(num==psArray[i].pnum){
					sn=i;
					break;
				}
			}
			if(sn>=0){
				boolean flag5=false;
				do{
					System.out.print("Whether to confirm the deletion��y/n����");
					Scanner sc1=new Scanner(System.in);
					String isgo=sc1.next();
					if(isgo.equalsIgnoreCase("y")||isgo.equalsIgnoreCase("n")){
						if(isgo.equalsIgnoreCase("y")){
							psArray[sn].pnum=-1;
							psArray[sn].pname="";
							psArray[sn].pprice=-1;
							//Delete product information, followed by information forward 1
							for(int j=sn;j<pcount-1;j++){
								psArray[j]=psArray[j+1];
							}
							flag=true;
							flag5=false;
						}else{
							flag=false;
							flag5=false;
						}
				}else{
					System.out.println("Please enter y or n!!!!!!!!!");
                        flag5=true;
				}
				}while(flag5);
//				System.out.print("Whether to confirm the deletion��y/n��");
//				if(sf2()){
//					psArray[sn].pnum=-1;
//				psArray[sn].pname="";
//				psArray[sn].pprice=-1;
//				//Delete product information, followed by information forward 1
//				for(int j=sn;j<pcount-1;j++){
//					psArray[j]=psArray[j+1];
//				}
//				flag=true;
//				}else{
//					flag=false;
//				}
//				
			}else
				flag=false;
		}while(flag1);
		
		return flag;
	}
	
	public boolean changeProducts(Products[] psArray,int pcount){
		
		boolean flag=false;
	
		boolean flag1=false;
		do{
			Scanner sc=new Scanner(System.in);
			System.out.print("Please enter the item number to be modified.");
			int num=sc.nextInt();
			int sn=-1;
			for(int i=0;i<pcount;i++){
				if(num==psArray[i].pnum){
					sn=i;
					break;
				}
			}
			if(sn>=0){
				System.out.print("Please enter a new item number.");
				int newnum=sc.nextInt();
				psArray[sn].pnum=newnum;
				System.out.print("Please enter the name of the new product.");
				String newname=sc.next();
				psArray[sn].pname=newname;
				System.out.print("Please enter the price of the new item at");
				double newprice=sc.nextDouble();
				psArray[sn].pprice=newprice;
				boolean flag5=false;
				do{
					System.out.print("Does the revision continue?��y/n��");
					Scanner sc1=new Scanner(System.in);
					String isgo=sc1.next();
					if(isgo.equalsIgnoreCase("y")||isgo.equalsIgnoreCase("n")){
						if(isgo.equalsIgnoreCase("y")){
							flag1=true;
							flag5=false;
						}else{
							flag1=false;
							flag=true;
							flag5=false;
						}
				}else{
					System.out.println("Please enter y or n !!!!!!!!!");
                        flag5=true;
				}
				}while(flag5);
//				System.out.print("Does the revision continue?��y/n��");
//				if(sf2()){
//					flag1=true;
//				}else{
//					flag1=false;
//					flag=true;
//				}
			}else{
				System.out.println("The item number you want to modify does not exist!");
				flag1=true;
			}
		}while(flag1);
		
		return flag;
	}
	
public void findAllProducts(Products[] psArray,int pcount){
		System.out.println("Product number\\t Product name\\t Product price");
		for(int i=0;i<pcount;i++){
			System.out.println(psArray[i].pnum+"\t"+psArray[i].pname+"\t"+psArray[i].pprice);
		}
	}
	
	
	public void findProductsByName(Products[] psArray,int pcount){
		Scanner sc=new Scanner(System.in);
		System.out.print("Please enter the item number to be queried.");
		int num=sc.nextInt();
		int sn=-1;
		for(int i=0;i<pcount;i++){
			if(num==psArray[i].pnum){
				sn=i;
				break;
			}
		}
		if(sn>=0){
			System.out.println("Product number\\t Product name\\t Product price");
			System.out.println(psArray[sn].pnum+"\t"+psArray[sn].pname+"\t"+psArray[sn].pprice);
		}else{
			System.out.println("The item number you are looking for does not exist!");
		}
		
	}
	
}
