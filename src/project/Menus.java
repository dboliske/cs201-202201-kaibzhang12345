//Role of this class:This category mainly includes the main menu display,user management menu,user query menu display function,product management menu and product inquiry menu
//Author:Zhang Kai
//Date:May 10th, 2022


package IITShopSystem;
import java.util.Arrays;
import java.util.Scanner;
public class Menus {
	int count=0;
	int pcount=0;
	UserInfo[] usArray=new UserInfo[10];
	Products[] psArray=new Products[10];
	
	

	public int getCount() {
		return count;
	}


	public void setCount(int count) {
		this.count = count;
	}


	public int getPcount() {
		return pcount;
	}


	public void setPcount(int pcount) {
		this.pcount = pcount;
	}


	public UserInfo[] getUsArray() {
		return usArray;
	}


	public void setUsArray(UserInfo[] usArray) {
		this.usArray = usArray;
	}


	public Products[] getPsArray() {
		return psArray;
	}


	public void setPsArray(Products[] psArray) {
		this.psArray = psArray;
	}


	
	
	@Override
	public String toString() {
		return "Menus [count=" + count + ", pcount=" + pcount + ", usArray=" + Arrays.toString(usArray) + ", psArray="
				+ Arrays.toString(psArray) + "]";
	}

	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + count;
		result = prime * result + pcount;
		result = prime * result + Arrays.hashCode(psArray);
		result = prime * result + Arrays.hashCode(usArray);
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Menus other = (Menus) obj;
		if (count != other.count)
			return false;
		if (pcount != other.pcount)
			return false;
		if (!Arrays.equals(psArray, other.psArray))
			return false;
		if (!Arrays.equals(usArray, other.usArray))
			return false;
		return true;
	}


	//Main Menu
	public void mainMenu(){
		boolean flag4=false;
		System.out.println("                             ******************************************************************");
		System.out.println("                             ************      Welcome to the IIT Shop System       ***********");
		System.out.println("                             ******************************************************************");
		do{
		System.out.println("1.Login (Product Management)");
		System.out.println("2.User Management");
		System.out.println("3.Exit");
		System.out.println("===============================================");
			System.out.print("Please enter the operation to be selected (1-3)");
		    Scanner sc=new Scanner(System.in);
			if(sc.hasNextInt()){
				int num=sc.nextInt();
				switch(num){
				case 1:
					UserInfo us=new UserInfo();
					if(us.login(usArray,count)){
						System.out.println("Login successful��");
						productsMenu();
					}else{
						if(count==0){
							System.out.println("No user registered yet, can't login, please register!");
							flag4=true;
						}else{
							flag4=true;
						}
					}
					break;
				case 2:
					userMenu();
					break;
				case 3:
					System.out.println("System exit��");
					System.exit(0);
					break;
				default :
					System.out.println("The number you entered is not in the (1-3) range, please re-enter the correct operating number!");
					System.out.println("===============================================");
					 flag4=true;					
					break;
				}
					 }else{
						 System.out.println("You entered a non-number, please re-enter the (1-3) range of correct operating numbers!");
						 System.out.println("===============================================");
						 flag4=true;		 
					 }
		}while(flag4);
		
		
 }


	/*
	 * User Management Menu
	 */
	public void userMenu(){
		
		boolean flag1=false;
		do{
			System.out.println("1.User additions");
			System.out.println("2.User Deletion");
			System.out.println("3.User Modification");
			System.out.println("4.User queries");
			System.out.println("5.Return to previous level");	
			System.out.println("===============================================");
			System.out.print("Please enter the action to be selected��1-5��");
			boolean flag=false;
		    Scanner sc=new Scanner(System.in);
			if(sc.hasNextInt()){
				int num=sc.nextInt();
				switch(num){
				case 1:
					boolean flag2=false;
					do{
						//Create new objects to prevent later information from overwriting earlier information
						UserInfo us=new UserInfo();
					  if(us.addUser(usArray, count)){
						System.out.println("Registration successful!");
						count++;
						System.out.println("========================================");
						boolean flag5=false;
						do{
								System.out.print("Whether to continue to register new users��y/n��");
								Scanner sc1=new Scanner(System.in);
								String isgo=sc1.next();
							if(isgo.equalsIgnoreCase("y")||isgo.equalsIgnoreCase("n")){
								if(isgo.equalsIgnoreCase("y")){
									flag2=true;
									flag5=false;
								}else{
									flag2=false;
							       flag1=true;
							       flag5=false;
								}
						}else{
							System.out.println("Please enter y or n!!!!!!!!!");
                                flag5=true;
						}
						}while(flag5);
												
					}else{
						flag2=true;
					}
					}while(flag2);
					
					break;
				case 2:
					boolean flag3=false;
					do{
						//Create new objects to prevent later information from overwriting earlier information
						UserInfo us=new UserInfo();
					if(us.deleteUsers(usArray, count)){
						System.out.println("Deleted successfully��");
						//Total number of users - 1
						count--;
						boolean flag5=false;
						do{
								System.out.print("Do you continue to delete users?��y/n��");
								Scanner sc1=new Scanner(System.in);
								String isgo=sc1.next();
							if(isgo.equalsIgnoreCase("y")||isgo.equalsIgnoreCase("n")){
								if(isgo.equalsIgnoreCase("y")){
									flag3=true;
									flag5=false;
								}else{
									flag3=false;
								   flag1=true;
								   flag5=false;
								}
						}else{
							System.out.println("Please enter y or n!!!!!!!!!!");
                                flag5=true;
						}
						}while(flag5);
					}else{						
						if(count==0){
							System.out.println("No user registered yet, can't delete, please register!");
							flag1=true;
							 flag=false;
							 flag3=false;
						}else{
							System.out.println("Delete failed!");
							boolean flag6=false;
							do{
									System.out.print("Do you continue to delete users?��y/n��");
									Scanner sc1=new Scanner(System.in);
									String isgo=sc1.next();
								if(isgo.equalsIgnoreCase("y")||isgo.equalsIgnoreCase("n")){
									if(isgo.equalsIgnoreCase("y")){
										flag3=true;
										flag6=false;
									}else{
										flag3=false;
									   flag1=true;
									   flag6=false;
									}
							}else{
								System.out.println("Please enter y or n!!!!!!!!!");
	                                flag6=true;
							}
							}while(flag6);
							//System.out.println("The username you want to delete does not exist or the password is incorrect!");
//						 flag1=true;
//						 flag=false;
//						 flag3=true;
						}
					}
					}while(flag3);
					break;
				case 3:
					//Create new objects to prevent later information from overwriting earlier information
					boolean flag7=false;
					if(count!=0){
						do{
						UserInfo us1=new UserInfo();
					if(us1.changeUsers(usArray, count)){
						System.out.println("Modified successfully!");
					}else{
						System.out.println("Modification failed!");
						boolean flag6=false;
						do{
							System.out.println("Whether to continue to modify��y/n��");
						String shifou=sc.next();
						if(shifou.equalsIgnoreCase("y")||shifou.equalsIgnoreCase("n")){
							if(shifou.equalsIgnoreCase("y")){
								flag6=false;
								flag7=true;
							}else{
								flag6=false;
								flag7=false;
								flag1=true;
							}
						}else{
							System.out.println("Please enter y or n����������");
							flag6=true;
						}
						}while(flag6);
					}
					}while(flag7);
					}else{
						System.out.println("No user registration yet, can't check, please register!");
						flag1=true;
					}
					
					
					break;
				case 4:
					if(count!=0){
						findUsersMenu();
					}else{
						System.out.println("No user registration yet, can't check, please register!");
						flag1=true;
						flag=false;
					}
					
					break;
				case 5:
					mainMenu();
					break;
					default :
						System.out.println("Enter a number not within (1-5), please re-enter (1-5) the correct operating number!");
						 flag1=true;
						break;
							}
					 }else{
						 System.out.println("You entered a non-number, please re-enter (1-5) the correct operating number!");
						 flag1=true;
					 }
		}while(flag1);
		
	}
	
	/*
	 * User Inquiry Menu
	 */
	public void findUsersMenu(){
		boolean flag1=false;
		do{
			System.out.println("1.Query all users");
			System.out.println("2.Search by username");
			System.out.println("3.Return to previous level");
			System.out.println("===============================================");
		
			boolean flag=false;
			UserInfo us=new UserInfo();
				System.out.println("Please enter the action to be selected��1-3��");
			    Scanner sc=new Scanner(System.in);
				if(sc.hasNextInt()){
					int num=sc.nextInt();
					switch(num){
					case 1:
						us.findAllUsers(usArray, count);
						flag1=true;
						break;
					case 2:
						us.findUsersByName(usArray, count);
						break;
					case 3:
						userMenu();
						break;
						default :
							System.out.println("Enter a number not within (1-3), please re-enter (1-3) the correct operating number!");
							 flag1=true;
							break;
					
								}
						 }else{
							 System.out.println("You entered a non-number, please re-enter (1-3) the correct operating number!");
							 flag1=true;
						 }
		}while(flag1);		
	}
	/*
	 * Product Management Menu
	 */
	public void productsMenu(){
		System.out.println("                             ***************************************************");
		System.out.println("                             ******************    Merchandise Management      *****************");
		System.out.println("                             ***************************************************");
		boolean flag1=false;
		do{
			System.out.println("1.Products added");
			System.out.println("2.Product Deletion");
			System.out.println("3.Product Modification");
			System.out.println("4.Product Search");
			System.out.println("5.Return to previous level");	
			System.out.println("===============================================");
			System.out.print("Please enter the operation to be selected (1-5)");
			boolean flag=false;
		    Scanner sc=new Scanner(System.in);
			if(sc.hasNextInt()){
				int num=sc.nextInt();
				switch(num){
				case 1:
					boolean flag2=false;
					do{
					
						Products ps=new Products();
					  if(ps.addProducts(psArray, pcount)){
						System.out.println("Add a new product successfully!");
						pcount++;
						System.out.println("========================================");
						//System.out.print("Whether to continue adding new products��y/n��");
						boolean flag5=false;
						do{
							System.out.print("Whether to continue adding new products��y/n��");
							Scanner sc1=new Scanner(System.in);
							String isgo=sc1.next();
							if(isgo.equalsIgnoreCase("y")||isgo.equalsIgnoreCase("n")){
								if(isgo.equalsIgnoreCase("y")){
									flag2=true;
									flag5=false;
								}else{
									flag2=false;
							        flag1=true;
							        flag5=false;
								}
						}else{
							System.out.println("������y��n!!!!!!!!!");
                                flag5=true;
						}
						}while(flag5);
//						if(sf()){
//							flag2=true;
//						}else{
//							flag2=false;
//							flag1=true;
//						}				
					}else{
						flag2=true;
					}
					}while(flag2);
					
					break;
				case 2:
					boolean flag3=false;
					if(pcount!=0){
						do{
						//Create new objects to prevent later information from overwriting earlier information
						Products ps=new Products();
					if(ps.deleteProducts(psArray, pcount)){
						System.out.println("Delete product successfully!");
						//Total number of products-1
						pcount--;
						boolean flag5=false;
						do{
							System.out.print("Do I continue to delete products?��y/n��");
							Scanner sc1=new Scanner(System.in);
							String isgo=sc1.next();
							if(isgo.equalsIgnoreCase("y")||isgo.equalsIgnoreCase("n")){
								if(isgo.equalsIgnoreCase("y")){
									flag3=true;
									flag5=false;
								}else{
									flag3=false;
							     	flag1=true;
							     	flag5=false;
								}
						}else{
							System.out.println("Please enter y or n !!!!!!!!!");
                                flag5=true;
						}
						}while(flag5);
					}else{
						System.out.println("Delete failed!");
						boolean flag6=false;
						do{
							System.out.print("Do you continue to delete items? (y/n)");
							Scanner sc1=new Scanner(System.in);
							String isgo=sc1.next();
							if(isgo.equalsIgnoreCase("y")||isgo.equalsIgnoreCase("n")){
								if(isgo.equalsIgnoreCase("y")){
									flag3=true;
									flag6=false;
								}else{
									flag3=false;
							     	flag1=true;
							     	flag6=false;
								}
						}else{
							System.out.println("Please enter y or n !!!!!!!!!");
                                flag6=true;
						}
						}while(flag6);
						//System.out.println("The item number you want to delete does not exist!");
						flag1=true;
					}
					}while(flag3);		
					}else{
						System.out.println("No new products have been added, you can't delete them, please add them!");
						flag1=true;
						flag=false;
					}
								
					break;
				case 3:
					//Create new objects to prevent later information from overwriting earlier information
					if(pcount!=0){
						Products ps1=new Products();
					if(ps1.changeProducts(psArray, pcount)){
						System.out.println("Modified successfully!");
					}
					}else{
						System.out.println("No new products have been added and cannot be modified, please add them!");
						flag1=true;
						flag=false;
					}
					
					break;
				case 4:
					if(pcount!=0){
						findProductsMenu();
					}else{
						System.out.println("No new products have been added, so you can't check, please add them!");
						flag1=true;
						flag=false;
					}
					break;
				case 5:
					mainMenu();
					break;
					default :
						System.out.println("Enter a number not within (1-5), please re-enter (1-5) the correct operating number!");
						 flag1=true;
						break;
							}
					 }else{
						 System.out.println("You have entered a non-number, please re-enter (1-5) the correct operating number!");
						 flag1=true;
					 }
		
		}while(flag1);
		
	}
	/*
	 * Product Search Menu
	 */
	public void findProductsMenu(){
		boolean flag1=false;
		do{
			System.out.println("1.Search all products");
			System.out.println("2.Search by product number");
			System.out.println("3.Return to previous level");
			System.out.println("===============================================");
			//Determine if the input is a number
			boolean flag=false;
			Products ps=new Products();
				System.out.println("Please enter the action to be selected��1-3��");
			    Scanner sc=new Scanner(System.in);
				if(sc.hasNextInt()){
					int num=sc.nextInt();
					switch(num){
					case 1:
						ps.findAllProducts(psArray, pcount);
						flag1=true;
						break;
					case 2:
						ps.findProductsByName(psArray, pcount);
						break;
					case 3:
						productsMenu();
						break;
						default :
							System.out.println("Enter a number not within (1-3), please re-enter (1-3) the correct operating number!");
							 flag1=true;
							break;
					
								}
						 }else{
							 System.out.println("You entered a non-number please re-enter (1-3) the correct operating number!");
							 flag1=true;
						 }
		}while(flag1);		
	}	
	
}
