//Role of this class:In the file mainly includes user login, user add, user delete, user modify, query all users, query by user name
//Author:Zhang Kai
//Date:May 10th, 2022



package IITShopSystem;
import java.util.Scanner;


public class UserInfo {
	public String uname;
	public String upass;
	
	
	
	
	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getUpass() {
		return upass;
	}

	public void setUpass(String upass) {
		this.upass = upass;
	}
	
	

	@Override
	public String toString() {
		return "UserInfo [uname=" + uname + ", upass=" + upass + "]";
	}


	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uname == null) ? 0 : uname.hashCode());
		result = prime * result + ((upass == null) ? 0 : upass.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserInfo other = (UserInfo) obj;
		if (uname == null) {
			if (other.uname != null)
				return false;
		} else if (!uname.equals(other.uname))
			return false;
		if (upass == null) {
			if (other.upass != null)
				return false;
		} else if (!upass.equals(other.upass))
			return false;
		return true;
	}

	/*
	 *User Login 
     */
	public boolean login(UserInfo[] usArray,int count){
		//Mark login as successful or not
		boolean flag=false;
		//Mark whether to log back in
		boolean flag1=false;
		//Number of login errors
		int times=0;
		Scanner sc=new Scanner(System.in);
		//Determine if a user is registered
		if(count!=0){
			do{
			System.out.print("Please enter the user name.");
		String name=sc.next();
		int sn=-1;
		for(int i=0;i<count;i++){
			if(name.equals((usArray)[i].uname)){
				sn=i;
				break;
			}
		}
		if(sn>=0){
			flag1=false;
			 System.out.print("Please enter the password.");
		     String pass=sc.next();
		     int sn1=-1;
		     for(int i=0;i<count;i++){
		    	 //Determine whether the user name has been registered and whether the password is correct
					if(usArray[i].uname.equals(name)&&usArray[i].upass.equals(pass)){
						sn1=i;
					}
				}
				if(sn1>=0){
					flag=true;
			          break;
				}else{
				
				times++;
			   System.out.println("Login failed, please login again!");
			   System.out.println("Password error \"+times+\" time");
			   //Login failed 3 times, end procedure
			   if(times==3){
				System.out.println("3 log-in errors, program ended!");
				System.exit(0);
				}
			flag1=true;
			flag=false;
		}
		}else{
			System.out.println("This username does not exist, please register!");
			flag=false;
		}
		}while(flag1);
		
		}else{
			flag=false;
		}
		
		
		
		return flag;
	}
	
	public boolean addUser(UserInfo[] usArray,int count){	 
		//Determine if the registered user name is successful
		boolean flag=false;
		//Determine if the password is the same as the confirmation password
		boolean flag1=false;
		//Mark if you want to re-register
		boolean flag2=false;
		Scanner sc=new Scanner(System.in);
		do{
			System.out.print("Please enter the user name.");
			String name=sc.next();
			int sn=-1;
			for(int i=0;i<count;i++){
				if(name.equals((usArray)[i].uname)){
					sn=i;
					break;
				}
			}
			if(sn>=0){
				System.out.println("This username already exists, please re-register!");
				flag2=true;
			}else{
				do{
				System.out.print("Please enter the password.");
				String pass=sc.next();
				System.out.print("Please enter the confirmation password at");
				String realpass=sc.next();
				if(pass.equals(realpass)){
					UserInfo us=new UserInfo();
					us.uname=name;
					us.upass=realpass;
					
					usArray[count]=us;
					flag=true;
					flag1=false;
					flag2=false;
				}else{
					System.out.println("The password is different from the confirmation password, please re-enter it!");
					flag1=true;
				}
		}while(flag1);
		}
	}while(flag2);
			
			
		return flag;
	}
	
	public boolean deleteUsers(UserInfo[] usArray,int count){
	
		boolean flag=false;
			Scanner sc=new Scanner(System.in);
			if(count!=0){
			System.out.print("Please enter the user name to be deleted.");
			String name=sc.next();
			System.out.print("Please enter the password.");
			String pass=sc.next();
			int sn=-1;
			for(int i=0;i<count;i++){
				if(name.equals((usArray)[i].uname)&&pass.equals(usArray[i].upass)){
					sn=i;
					break;
				}
			}
			if(sn>=0){
				boolean flag5=false;
				do{
					System.out.print("Is the deletion confirmed?��y/n��");
					Scanner sc1=new Scanner(System.in);
					String isgo=sc1.next();
					if(isgo.equalsIgnoreCase("y")||isgo.equalsIgnoreCase("n")){
						if(isgo.equalsIgnoreCase("y")){
							usArray[sn].uname="";
							usArray[sn].upass="";
							//Delete user information, followed by information forward 1
							for(int j=sn;j<count-1;j++){
								usArray[j]=usArray[j+1];
							}
							flag=true;
							flag5=false;
						}else{
							//System.out.println("Delete failed!");
							flag5=false;
							flag=false;
						}
				}else{
					System.out.println("Please enter y or n !!!!!!!!!");
                        flag5=true;
				}
				}while(flag5);		
		}else{
			flag=false;
		}
	}else{
		flag=false;
		}	
		return flag;
	}
	
	public boolean changeUsers(UserInfo[] usArray,int count){
	
		boolean flag=false;
	
		boolean flag1=false;
	
		boolean flag2=false;
			do{
			Scanner sc=new Scanner(System.in);
			System.out.print("Please enter the user name to be modified at");
			String name=sc.next();
			int sn=-1;
			for(int i=0;i<count;i++){
				if(name.equals((usArray)[i].uname)){
					sn=i;
					break;
				}
			}
			if(sn>=0){
				System.out.print("Please enter a new username at");
				String newname=sc.next();
				do{
				System.out.print("Please enter a new password at");
				String newpass=sc.next();
				System.out.print("Please enter a new confirmation password at");
				String newrealpass=sc.next();
				if(newpass.equals(newrealpass)){
					usArray[sn].uname=newname;
					usArray[sn].upass=newrealpass;
					flag=true;
					flag2=false;
//					boolean flag5=false;
//					do{
//						System.out.print("Does the revision continue?��y/n��");
//						Scanner sc1=new Scanner(System.in);
//						String isgo=sc1.next();
//						if(isgo.equalsIgnoreCase("y")||isgo.equalsIgnoreCase("n")){
//							if(isgo.equalsIgnoreCase("y")){
//								flag1=true;
//								flag5=false;
//							}else{
//								flag1=false;
//								flag=true;
//								flag2=false;
//								flag5=false;
//							}
//					}else{
//						System.out.println("Please enter y or n !!!!!!!!!");
//	                        flag5=true;
//					}
//					}while(flag5);
				}else{
					System.out.println("New password and new confirmation password are incorrect, please re-enter!");
					flag2=true;
				}
			}while(flag2);
			}else{
				System.out.println("The username you want to change does not exist or the password is incorrect!");
				flag1=true;	
	        }
		}while(flag1);	
		return flag;
	}
	
public void findAllUsers(UserInfo[] usArray,int count){
		
			System.out.println("Username\\t Password");
		   for(int i=0;i<count;i++){
			System.out.println(usArray[i].uname+"\t"+usArray[i].upass);
		}	
	}
	
	
	public void findUsersByName(UserInfo[] usArray,int count){
			Scanner sc=new Scanner(System.in);
		System.out.print("Please enter the user name to be queried at");
		String name=sc.next();
		int sn=-1;
		for(int i=0;i<count;i++){
			if(name.equals((usArray)[i].uname)){
				sn=i;
				break;
			}
		}
		if(sn>=0){
			System.out.println("Username\\t Password");
			System.out.println(usArray[sn].uname+"\t"+usArray[sn].upass);
		}else{
			System.out.println("The username you want to query does not exist or the password is incorrect!");
		}			
	}
	
}
