# Final Project

## Total

68/150

## Break Down

Phase 1:                                                            16/50

- Description of the user interface                                 3/3
- Description of the programmer's tasks:
  - Describe how you will read the input                            0/3
  - Describe how you will process the data from the input file      0/4
  - Describe how you will store the data                            3/3
  - How will you add/delete/modify data?                            5/5
  - How will you search data?                                       5/5
- Classes: List of names and descriptions                           0/7
- UML Class Diagrams                                                0/10
- Testing Plan                                                      0/10

Phase 2:                                                            52/100

- Compiles and runs with no run-time errors                         10/10
- Documentation                                                     5/15
- Test plan                                                         0/10
- Inheritance relationship                                          0/5
- Association relationship                                          5/5
- Searching works                                                   5/5
- Uses a list                                                       5/5
- Project reads data from a file                                    0/5
- Project writes data to a file                                     0/5
- Project adds, deletes, and modifies data stored in list           5/5
- Project generates paths between any two stations                  10/10
- Project encapsulates data                                         5/5
- Project is well coded with good design                            2/10
- All classes are complete (getters, setters, toString, equals...)  0/5

## Comments

### Design Comments

- You seems to misunderstand all questions, so most of your answers are wrong.  -7
- No class name and related description. -7
- No UML Class Diagrams. -10
- No test plan. -10 

### Code Comments

- You didn't understand all requirements of this project. Most implementations are useless.  -10
- No test plan. -10
- The project didn't represent the inheritance relationship.  -5
- No read and write file. -10
- The project did wrong.  -8
- All classes are incomplete. -5