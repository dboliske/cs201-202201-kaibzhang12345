# Midterm Exam

## Total

70/100

## Break Down

1. Data Types:                  16/20
    - Compiles:                 5/5
    - Input:                    3/5
    - Data Types:               3/5
    - Results:                  5/5
2. Selection:                   18/20
    - Compiles:                 5/5
    - Selection Structure:      10/10
    - Results:                  3/5
3. Repetition:                  18/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               3/5
4. Arrays:                      6/20
    - Compiles:                 5/5
    - Array:                    0/5
    - Exit:                     1/5
    - Results:                  0/5
5. Objects:                     12/20
    - Variables:                5/5
    - Constructors:             2/5
    - Accessors and Mutators:   3/5
    - toString and equals:      2/5

## Comments

1. Needed to confirm that the user input is an integer and need to convert the result to a character, not a String.
2. Same input issue as Question 1.
3. Same issue as Question 1 and 2.
4. Does not correctly prompt for words or use arrays.
5. Constructors do not initialize all instance variables, `setAge` does not validate `age` before setting it, and the `equals` method does not properly compare instance variables.
