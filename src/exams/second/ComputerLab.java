package Final_Exam;

public class ComputerLab extends Classroom{

	 private boolean computers;

	public ComputerLab(boolean computers) {
		super();
		this.computers = computers;
	}

	
	
	public void setComputers(boolean computers) {
		this.computers = computers;
	}



	@Override
	public String toString() {
		return "ComputerLab [computers=" + computers + "]";
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (computers ? 1231 : 1237);
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComputerLab other = (ComputerLab) obj;
		if (computers != other.computers)
			return false;
		return true;
	}
	 
	
	
	 

}
