package Final_Exam;

public class Polygon {
    public double width;
    public double height;
    public double radius;
    public double area;
    public double Perimeter;
	protected String name;
	
	
	public double areaPolygon() {
		return area;
	}
	
    public double perimergraphical() {
    	return Perimeter;
    }
	
	public Polygon(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Polygon [name=" + name + "]";
	}
	
	
	
	
}
