package Final_Exam;


public class Circle extends Polygon{
	
	private double radius;

	public double area() {
		area = Math.PI * radius * radius;
		return area;
		
	}
	
	
	public double Perimeter() {
		Perimeter = 2.0 * Math.PI * radius;
		return Perimeter;
		
	}
	
	public Circle(String name, double radius) {
		super(name);
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	@Override
	public String toString() {
		return "Circle [radius=" + radius + "]";
	}
	
	
	
	

	
}
