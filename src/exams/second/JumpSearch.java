package Final_Exam;


public class JumpSearch {
	 
	  /**
	   * 
	   * @param ds
	   * @param elementToSearch
	   * @return
	   */
	  public static double jumpSearch(double[] ds, double elementToSearch) {
	 
	    double arrayLength = ds.length;
	    double jumpStep = (int) Math.sqrt(ds.length);
	    double previousStep = 0;
	 
	    while (ds[(int) (Math.min(jumpStep, arrayLength) - 1)] < elementToSearch) {
	      previousStep = jumpStep;
	      jumpStep += (int)(Math.sqrt(arrayLength));
	      if (previousStep >= arrayLength)
	        return -1;
	    }
	    while (ds[(int) previousStep] < elementToSearch) {
	      previousStep++;
	      if (previousStep == Math.min(jumpStep, arrayLength))
	        return -1;
	    }
	 
	    if (ds[(int) previousStep] == elementToSearch)
	      return previousStep;
	    return -1;
	  }
	 
	  /**
	   * 
	   * @param d
	   * @param index
	   */
	  public static void print(double d, int index) {
	    if (index == -1){
	      System.out.println(d + " Not found");
	    }
	    else {
	      System.out.println(d + " Find at the index: " + index);
	    }
	  }
	 
	  
	  
	  public static void main(String[] args) {
	    int index = (int) jumpSearch(new double[]{0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142}, 3.144);
	    print(3.144, index);
	  }
	}





