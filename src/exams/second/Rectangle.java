package Final_Exam;

public class Rectangle extends Polygon{
	

	private double width;
	
	private double height;
	
	public double area() {
		area = width * height;
		return area;
		
	}
	
	public double Perimeter() {
		Perimeter = (width * height) * 2;
		return Perimeter;
		
	}

	public Rectangle(String name, double width, double height) {
		super(name);
		this.width = width;
		this.height = height;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "Rectangle [width=" + width + ", height=" + height + "]";
	}
	
	
	

	
}
