# Final Exam

## Total

72/100

## Break Down

1. Inheritance/Polymorphism:    17/20
    - Superclass:               5/5
    - Subclass:                 5/5
    - Variables:                5/5
    - Methods:                  2/5
2. Abstract Classes:            12/20
    - Superclass:               0/5
    - Subclasses:               4/5
    - Variables:                4/5
    - Methods:                  4/5
3. ArrayLists:                  14/20
    - Compiles:                 5/5
    - ArrayList:                3/5
    - Exits:                    2/5
    - Results:                  4/5
4. Sorting Algorithms:          15/20
    - Compiles:                 5/5
    - Selection Sort:           5/10
    - Results:                  5/5
5. Searching Algorithms:        14/20
    - Compiles:                 5/5
    - Jump Search:              5/10
    - Results:                  4/5

## Comments

1. The superclass didn't implement Classroom() constructor. -1
   Didn't check the validation of parameter seats. -1
   The subclass didn't implement the hasComputer() method. -1
   
2. The code didn't show that Polygen is an abstract class; -5
   Didn't show area() and perimeter() are abstract methods; -1
   Too many attributes for Class Polygen; -1
   No override identifier in area() and perimeter() function in subclass. -1
   
3. The program uses an array to store the input value instead of ArrayList, -2; 
   didn't implement Exit function -3; 
   didn't handle with the exception -1; 
   
4. didn't use select sort algorithm to implement sort function -5.

5. did't  allows the user to search for a value -1; 
   didn't implement jump search with recursive -5.
