# Lab 0

## Total

20/20

## Break Down

* Eclipse "Hello World" program         -/5
* Correct TryVariables.java & run       -/4
* Name and Birthdate program            -/5
* Square
  * Pseudocode                          -/2
  * Correct output matches pseudocode   -/2
* Documentation                         -/2

## Comments

Didn't deduct any points, but make sure you use the correct package declaration at the top of your classes. In the case of this lab, it should be package labs.lab0
