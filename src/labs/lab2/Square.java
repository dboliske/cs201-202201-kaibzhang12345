//KaiZhang_CS201_Section03_2/18/2022_Lab2/exercise1/print out a square 
package Lab2;

import java.util.Scanner;
 
public class Square {
	public static void main(String[] args) {
		System.out.println("Please enter the number of rows");
		Scanner sc = new Scanner(System.in);
		if (sc.hasNextInt()) {
			int rownum = sc.nextInt();
			// solidSquare
			printSolidSquare(rownum);

			
		} else {
			System.out.println("Please enter a positive integer greater than 1");
		}
	}
 
	/**
	 * print out solid square
	 * 
	 * 
	 */
	
	
	
	private static void printSolidSquare(int rownum) {
		if (rownum > 1) {
			for (int i = 1; i <= rownum; i++) {
				for (int j = 1; j <= rownum; j++) {
					System.out.print("*");
				}
				System.out.println();
			}
		} else {
			System.out.println("The number should be greater than 1");
		}
	}
 
	 
	 
	 
	 
	 
	 
	
	
}

		
	
 