# Lab 5

## Total

7/20

## Break Down

CTAStation

- Variables:                    2/2
- Constructors:                 0/1
- Accessors:                    2/2
- Mutators:                     1/2
- toString:                     1/1
- equals:                       2/2

CTAStopApp

- Reads in data:                0/2
- Loops to display menu:        0/2
- Displays station names:       0/1
- Displays stations by access:  0/2
- Displays nearest station:     0/2
- Exits                         0/1

## Comments
- CTAStation didn't inheritance GeoStation; didn't check the validation of lat and lng; didn't follow the UML to implement -3
- The programmer didn't understand the requirements of the homework, most of the codes were wrong. -10
