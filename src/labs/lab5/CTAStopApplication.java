package Labs_Lab5;
import java.io.File;
import java.util.Scanner;

public class CTAStopApplication {

	
	public static CTAStopRecord[] readData(String filename) {
		CTAStopRecord[] data = new CTAStopRecord[10];
		int count = 0;
		
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine(); // 2022-01-01,20,40
					String[] values = line.split(","); // {2022-01-01, 20, 40}
					String[] dateValues = values[0].split("-");
					GeoLocation d = new GeoLocation(
						Integer.parseInt(dateValues[0]),
						Integer.parseInt(dateValues[1])
					);
					double low = Double.parseDouble(values[1]);
					double high = Double.parseDouble(values[2]);
					
					CTAStopRecord record = new CTAStopRecord(d, low, high); // new record
					
					if (count == data.length) { // need to increase array size
						data = resize(data, 2 * data.length);
					}
					
					data[count] = record;
					count++;
				} catch (Exception e) {
					// a line failed to be read in
				}
			}
			
			input.close();
		} catch (Exception e) {
			System.out.println("Something went wrong reading in our data.");
		}
		
		// Trim array to exact size
		data = resize(data, count);
		
		return data;
	}

	private static CTAStopRecord[] resize(CTAStopRecord[] data, int i) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	/*显示菜单选项，它应该是以下内容。
    显示站名
    显示有/无轮椅通道的车站
    显示最近的车站
    退出*/
	
	
	public static CTAStopRecord[] resize1(CTAStopRecord[] data, int size) {
		CTAStopRecord[] temp = new CTAStopRecord[size];
		int limit = data.length < size ? data.length: size;
		for (int i=0; i<limit; i++) {
			temp[i] = data[i];
		}
		
		return temp;
	}
	
	public static void menu(CTAStopRecord[] name) {
		Scanner input = new Scanner(System.in);
		boolean done = false;
		
		do {
			System.out.println("1.  Show station name");
			System.out.println("2. Show stations with/without wheelchair access");
			System.out.println("3. Show nearest stations");
			System.out.println("4. Exit");
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			// menu
			switch (choice) {
				// 1. station name
				case "1":
					stationName(name);
					break;
				// 2.stations with/without wheelchair access
				case "2":
					 wheelchairaccess1(name);
					break;
				// 3. nearest stations
				case "3":
					neareststations(name);
					break;
				// 4. exit
				case "4":
					done = true;
					break;
				default:
					System.out.println("I'm sorry, but I can't do that.");
			}
			
		} while (!done);
		
		input.close();
	}
	
	private static void neareststations(CTAStopRecord[] name) {
		// TODO Auto-generated method stub
		
	}

	private static void wheelchairaccess1(CTAStopRecord[] name) {
		// TODO Auto-generated method stub
		
	}

	public static String stationName(CTAStopRecord[] name) {
		if (name.length == 0) {
			System.out.println("No data available.");
		} else {
			
			for (int i=1; i<name.length; i++) {
				
				}
			}
			
			int m = 0;
			System.out.println("station name was " + name[m].getName() + " and occurred on " + readData(null)[m].getName());
		}

	
	public static void wheelchairaccess(CTAStopRecord[] name) {
		if (name.length == 0) {
			System.out.println("No data available.");
		
				
			}
			
			int m = 0;
			Object[] name1 = null;
			System.out.println("wheelchairaccess " + ((CTAStopRecord) name1[m]).getWheelchair() + " and occurred on " + ((CTAStopRecord) name1[m]).getWheelchair());
		}
	
	
	public static void average(CTAStopRecord[] data) {
		if (data.length == 0) {
			System.out.println("No data available.");
		} else {
			double total = 0;
			for (int i=0; i<data.length; i++) {
				double average = (data[i].getName() + data[i].getWheelchair()) / 2.0;
				total = total + average;
			}
			double average = total / data.length;
			System.out.println("Average temperature was " + average);
		}
	}
	
	public static void main(String[] args) {
		// Read in data
		CTAStopRecord[] data = readData("src/labs/lab5/CTAStops.csv");
		menu(data);
		
		System.out.println("Program ending...");
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}




