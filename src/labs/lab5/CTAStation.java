package Labs_Lab5;

public class CTAStation {

	   private String name;
	   private String location;
	   private boolean wheelchair;
	   private boolean open;

	   
	   public CTAStation() {
		   name = "zhang";
		   location = "califonia";
		   wheelchair = true;
		   open = true;
		}
	   
	
	   public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
			String n = null;
			name = n;
			lat = 22;
			lng = -87;
			getLocation();
			wheelchair = (true);
			setOpen(true);
			
		}
	
	
	   public String getName() {
			return name;
		}

	   public String getLocation() {
		return location;
	}

	   
	   
	   
	public void setName(String name) {
			this.name = name;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}


	public boolean isWheelchair() {
		return wheelchair;
	}


	public void setOpen(boolean open) {
		this.open = open;
	}


	public boolean isOpen() {
		return open;
	}


	@Override
	public String toString() {
		return "CTAStation [name=" + name + ", location=" + location + ", wheelchair=" + wheelchair + ", open=" + open
				+ "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (open ? 1231 : 1237);
		result = prime * result + (wheelchair ? 1231 : 1237);
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CTAStation other = (CTAStation) obj;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (open != other.open)
			return false;
		if (wheelchair != other.wheelchair)
			return false;
		return true;
	}


	

	

	


	
	
	
	
	
	
	
	

	
	
}
