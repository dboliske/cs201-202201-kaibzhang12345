package Labs_Lab5;

public class CTAStopsRecord {

	
	private GeoLocation name;
	private double lat;
	private double lng;
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	
	public CTAStopsRecord() {
		name = new GeoLocation();
		lat= 0.0;
		lng = 0.0;
		location = "";
		wheelchair = true;
		open = true;
	}
	
	public CTAStopsRecord(GeoLocation name, double lat, double lng,String location,boolean wheelchair,boolean open) {
		this.name = name;
		this.lat = lat;
		this.lng = lng;
		this.location = location;
		this.wheelchair = wheelchair;
		this.open = open;
	}
	
	

	public GeoLocation getName() {
		return name;
	}

	public void setName(GeoLocation name) {
		this.name = name;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public boolean isWheelchair() {
		return wheelchair;
	}

	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	
	
	public boolean equals(CTAStopsRecord cr) {
		return this.name.equals(cr.getName()) && this.lat == cr.getLat() && this.lng == cr.getLng()&& this.location == cr.getLocation()
				&&this.wheelchair == cr.isWheelchair() &&this.open == cr.isOpen();
	}
	
	
	public String toString() {
		return this.name.getLat() + "-" +
			(this.name.getLat()< 42?("0" + this.name.getLat()): this.name.getLat()) + "-" +
			(this.name.getLng()>-88?("0" + this.name.getLng()): this.name.getLng()) + "," +
			(this.name.getLocation()=="elevated"?("0" + this.name.getLocation()): this.name.getLocation()) + "," +
			(this.name.getWheelchair() == "true"?("0" + this.name.getWheelchair()): this.name.getWheelchair()) + "," +
			(this.name.getOpen() == "true" ?("0" + this.name.getOpen()): this.name.getOpen()) + "," +
			lat + "," + lng+"," + location + "," + wheelchair + "," + open;
	}
	
	
	
	
	
	/*public void setDate(DateTime date) {
		this.date = date;
	}
	
	public void setLow(double low) {
		this.low = low;
	}
	
	public void setHigh(double high) {
		this.high = high;
	}
	
	public DateTime getDate() {
		return date;
	}
	
	public double getLow() {
		return low;
	}
	
	public double getHigh() {
		return high;
	}
	
	public boolean equals(WeatherRecord wr) {
		return this.date.equals(wr.getDate()) && this.low == wr.getLow() && this.high == wr.getHigh();
	}
	
	public String toString() {
		return this.date.getYear() + "-" +
			(this.date.getMonth()<10?("0" + this.date.getMonth()): this.date.getMonth()) + "-" +
			(this.date.getDay()<10?("0" + this.date.getDay()): this.date.getDay()) + "," +
			low + "," + high;
	}
	
	*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
