# Lab 7

## Total

13/20

## Break Down

- Exercise 1 3/4
- Exercise 2 5/5
- Exercise 3 3/5
- Exercise 4 2/6

## Comments
- In exercise 1, the codes to deal with data input are wrong. -1
- Exercise 3 didn't implement select sort algorithm -2
- In exercise 4, didn't implement binary search with recursively; didn't allow users to input the search value. -4


