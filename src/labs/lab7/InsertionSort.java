package Labs_Lab7;









public class InsertionSort {
	public void insertSort(Comparable[] data){					
		for(int i=1;i<data.length;i++){
		   Comparable<Comparable<Comparable>> key=data[i];
		   int j=i;
		  
		   while(j>0&&key.compareTo(data[j-1])<0){
			   
			   data[j]=data[j-1];
			   j--;
		   }
		   data[j]=key;
		}
	}
	
	public void show(Comparable[] data){
		for(int i=0;i<data.length;i++){
			System.out.println(data[i]);
		}
	}
	public static void main(String[] args)
	{
	
		Comparable[] data2={"cat", "fat", "dog", "apple", "bat", "egg"};	
		InsertionSort insert=new InsertionSort();
		
	
		
		System.out.println("The result of sorting the strings from smallest to largest is��");
		insert.insertSort(data2);
		insert.show(data2);
 
	}
 
}
