package Labs_Lab7;



public class BinarySearch  {  
	
	
	 static int bsearch( String[] a, String v ) {  
	  int l, r;  
	  l = 0; r = a.length-1;  
	  while ( l <= r ) {  
	  int m = (l+r)/2;  
	  if ( a[m].compareTo(v)==0 ) return m; else  
	  if ( a[m].compareTo(v)>0 ) r = m-1; else  
	  if ( a[m].compareTo(v)<0 ) l = m+1;  
	  }  
	  return -1;  
	  }  
	 public static void main(String[] args) {  
	    String str[] = {"c", "html", "java", "python", "ruby", "scala"};
	    
	    int bsearch = bsearch(str, "c");  
	    System.out.println(bsearch);  
	 }  
}