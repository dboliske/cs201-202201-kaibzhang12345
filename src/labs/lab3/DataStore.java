//KaiZhang_CS201_Section03_2/18/2022_Lab3/exercise2/storing them in an array
package Lab3;

import java.util.Scanner;
import java.util.InputMismatchException;
 
public class DataStore {
	
	public static void main(String[] args) {
		
		Scanner scan = null;
		try {
			scan = new Scanner(System.in);
			System.out.print( "Please enter the number: " );
			int inputNum = scan.nextInt();
			if( inputNum <= 0 ) {
				throw new Exception( "enter error" );
			}
			
			System.out.println( "please enter the number: " );
			int arr[] = new int[inputNum];
			int num = 0;
			int count = 0;
			while( count < inputNum ) {
				num = scan.nextInt();
				arr[count] = num;
				count++;
			}
			
			for( int i = 0; i < arr.length; i++ ) {
				System.out.print( arr[i] + "  " );
			}
		} catch ( Exception e ) {
			throw new InputMismatchException( "\u8f93\u5165\u6709\u8bef\u002c\u0020\u8bf7\u91cd\u65b0\u8f93\u5165" );
		} finally {
			try {
				if ( scan != null ) {
					scan.close();
				} 
			} catch ( Exception e2 ) {
				e2.printStackTrace();
			}
		}
		
	}
 
}




