//KaiZhang_CS201_Section03_2/18/2022_Lab3/exercise01/reads in the file
package Lab3;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
 
public class Readcsvfile {
 
    public static void main(String[] args) {
 
        String csvFile = "C:\\IIT\\Gitkraken\\cs201-202201-kaibzhang12345\\src\\labs\\lab3\\grades.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
 
        try {
 
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
 
                // use comma as separator
                String[] country = line.split(cvsSplitBy);
 
                System.out.println();
 
            }
 
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
 
    }
 
}
