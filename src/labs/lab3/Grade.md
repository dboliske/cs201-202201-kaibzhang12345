# Lab 3

## Total

17/20

## Break Down

* Exercise 1    5/6
* Exercise 2    6/6
* Exercise 3    6/6
* Documentation 0/2

## Comments

Please fix package declarations in the future. Additionally, no documentation was included and you should be using relative, not absolute file paths.
