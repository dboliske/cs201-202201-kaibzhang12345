# Lab 1

## Total

17/20

## Break Down

* Exercise 1    2/2
* Exercise 2    2/2
* Exercise 3    2/2
* Exercise 4
  * Program     2/2
  * Test Plan   0/1
* Exercise 5
  * Program     2/2
  * Test Plan   0/1
* Exercise 6
  * Program     2/2
  * Test Plan   0/1
* Documentation 5/5

## Comments

Same comment about package declarations as with previous lab. Additionally, no test results were included for exercises 4-6. Finally, while I still graded it, please be careful with commenting out code as your Arithmatic_calculations.java class contains no runnable code.
