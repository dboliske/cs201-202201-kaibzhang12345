//KaiZhang_CS201_Section03_2/18/2022_Lab1/exercise4/Prompt the user for a temperature

// convert the Celsius to Fahrenheit
/*package Lab1;

import java.util.Scanner;

public class ConvertTemperature {
    public static void main(String[] args) {
        System.out.print("please enter temperature to be converted(Unit: degree Celsius)：");
        Scanner in = new Scanner(System.in);     //  Get the console input
        double celsius = in.nextDouble();     // Get the Celsius temperature entered by the user
        ConvertTemperature converter = new ConvertTemperature();      // Creating objects of a class
        double fahrenheit = converter.getFahrenheit(celsius);      // Conversion temperature in degrees Fahrenheit
        System.out.println("Temperature at which conversion is completed (Unit: degree Fahrenheit)：" + fahrenheit);// 输出转换结果 Output conversion results
    }
         //  Converting degrees Celsius to Fahrenheit
    public double getFahrenheit(double celsius) {
        double fahrenheit = 1.8 * celsius + 32;     // Calculate Fahrenheit Temperature
        return fahrenheit;//  Return Fahrenheit Temperature
    }
}*/




//convert the Fahrenheit to Celsius
package Lab1;

import java.util.Scanner;

public class ConvertTemperature {
    public static void main(String[] args) {
        System.out.print("please enter temperature to be converted(Unit: degree fahrenheit)：");
        Scanner in = new Scanner(System.in);     //  Get the console input
        double fahrenheit = in.nextDouble();     // Get the fahrenheit temperature entered by the user
        ConvertTemperature converter = new ConvertTemperature();     // Creating objects of a class
        double celsius = converter.getcelsius(fahrenheit);      // version temperature in degrees  celsius
        System.out.println("Temperature at which conversion is completed (Unit: degree celsius)：" + celsius);     // 输出转换结果 Output conversion results
    }
    //  Converting degrees Fahrenheit to Celsius
    public double getcelsius(double fahrenheit) {
        double celsius = (fahrenheit - 32)/1.8;     // Calculate Celsius temperature
        return celsius;     // return Celsius temperature
    }
}
